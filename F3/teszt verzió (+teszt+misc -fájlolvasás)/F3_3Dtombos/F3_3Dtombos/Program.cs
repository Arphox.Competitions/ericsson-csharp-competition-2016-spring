﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;

namespace F3_3Dtombos
{
    class Program
    {
        static void Main(string[] args)
        {

        }
    }
    interface IMaxProduct
    {
        BigInteger GetMax3x3x3ProductInCuboid(string filePath);
    }

    class OzsvartKaroly_F3 : IMaxProduct
    {
        int[,,] matrix;
        object lockObject = new object();

        public BigInteger GetMax3x3x3ProductInCuboid(string filePath)
        {
            BigInteger max = 0;
            int xLength = matrix.GetLength(0) - 1;
            int yLength = matrix.GetLength(1) - 1;
            int zLength = matrix.GetLength(2) - 1;

            List<Task> tasks = new List<Task>();

            for (int z = 1; z < zLength; z++)
            {
                for (int y = 1; y < yLength; y++)
                {
                    int zLocal = z;
                    int yLocal = y;
                    tasks.Add(Task.Factory.StartNew(() =>
                    {
                        for (int x = 1; x < xLength; x++)
                        {
                            BigInteger sub3x3Prod = GetSub3x3ElementsPRODUCT(x, yLocal, zLocal);
                            lock (lockObject)
                            {
                                if (sub3x3Prod > max)
                                    max = sub3x3Prod;
                            }
                        }
                    }
                        ));
                }
            }

            Task.WaitAll(tasks.ToArray());
            return max;
        }
        BigInteger GetSub3x3ElementsPRODUCT(int centerX, int centerY, int centerZ)
        {
            return
                //HORIZONTAL LAYER TO THE LEFT   (centerX - 1)
                (BigInteger)matrix[centerX - 1, centerY - 1, centerZ - 1] *
                matrix[centerX - 1, centerY - 1, centerZ] *
                matrix[centerX - 1, centerY - 1, centerZ + 1] *

                matrix[centerX - 1, centerY, centerZ - 1] *
                matrix[centerX - 1, centerY, centerZ] *
                matrix[centerX - 1, centerY, centerZ + 1] *

                matrix[centerX - 1, centerY + 1, centerZ - 1] *
                matrix[centerX - 1, centerY + 1, centerZ] *
                matrix[centerX - 1, centerY + 1, centerZ + 1] *

                //HORIZONTAL MIDDLE LAYER        (centerX)
                matrix[centerX, centerY - 1, centerZ - 1] *
                matrix[centerX, centerY - 1, centerZ] *
                matrix[centerX, centerY - 1, centerZ + 1] *

                matrix[centerX, centerY, centerZ - 1] *
                matrix[centerX, centerY, centerZ] *
                matrix[centerX, centerY, centerZ + 1] *

                matrix[centerX, centerY + 1, centerZ - 1] *
                matrix[centerX, centerY + 1, centerZ] *
                matrix[centerX, centerY + 1, centerZ + 1] *

                //HORIZONTAL LAYER TO THE RIGHT  (centerX + 1)
                matrix[centerX + 1, centerY - 1, centerZ - 1] *
                matrix[centerX + 1, centerY - 1, centerZ] *
                matrix[centerX + 1, centerY - 1, centerZ + 1] *

                matrix[centerX + 1, centerY, centerZ - 1] *
                matrix[centerX + 1, centerY, centerZ] *
                matrix[centerX + 1, centerY, centerZ + 1] *

                matrix[centerX + 1, centerY + 1, centerZ - 1] *
                matrix[centerX + 1, centerY + 1, centerZ] *
                matrix[centerX + 1, centerY + 1, centerZ + 1];
        }

        #region Other versions which were slower in tests
        //public BigInteger Iterate1()
        //{
        //    //int maxX = 0, maxY = 0, maxZ = 0;

        //    for (int z = 1; z < Matrix.GetLength(2) - 1; z++)
        //    {
        //        for (int y = 1; y < Matrix.GetLength(1) - 1; y++)
        //        {
        //            for (int x = 1; x < Matrix.GetLength(0) - 1; x++)
        //            {
        //                BigInteger sub3x3Prod = GetSub3x3ElementsPRODUCT(x, y, z);

        //                if (sub3x3Prod > max)
        //                {
        //                    max = sub3x3Prod;

        //                    //maxX = x;
        //                    //maxY = y;
        //                    //maxZ = z;
        //                }
        //            }
        //        }
        //    }

        //    //Console.WriteLine("Maximum: x = {0}, y = {1}, z = {2}", maxX, maxY, maxZ);
        //    //Console.WriteLine("Value = " + max);
        //    return max;
        //} //First version
        //public BigInteger Iterate2()
        //{
        //    BigInteger max = 0;
        //    int xLength = Matrix.GetLength(0) - 1;
        //    int yLength = Matrix.GetLength(1) - 1;
        //    int zLength = Matrix.GetLength(2) - 1;

        //    for (int z = 1; z < zLength; z++)
        //    {
        //        for (int y = 1; y < yLength; y++)
        //        {
        //            for (int x = 1; x < xLength; x++)
        //            {
        //                BigInteger sub3x3Prod = GetSub3x3ElementsPRODUCT(x, y, z);

        //                if (sub3x3Prod > max)
        //                {
        //                    max = sub3x3Prod;
        //                }
        //            }
        //        }
        //    }

        //    return max;
        //} //Length variables outside
        //public BigInteger Iterate3()
        //{
        //    BigInteger max = 0;
        //    //int maxX = 0, maxY = 0, maxZ = 0;
        //    int xLength = Matrix.GetLength(0) - 1;
        //    int yLength = Matrix.GetLength(1) - 1;
        //    int zLength = Matrix.GetLength(2) - 1;

        //    for (int z = 1; z < zLength; z++)
        //    {
        //        for (int y = 1; y < yLength; y++)
        //        {
        //            int zLocal = z;
        //            int yLocal = y;

        //            Parallel.For(
        //            1, xLength, (x) =>
        //            {
        //                BigInteger sub3x3Prod = GetSub3x3ElementsPRODUCT(x, yLocal, zLocal);
        //                lock (lockObject)
        //                {
        //                    if (sub3x3Prod > max)
        //                        max = sub3x3Prod;
        //                }
        //            }
        //            );
        //        }
        //    }
        //    return max;
        //} //Parallel.For for each X
        #endregion
    }


    class Misc
    {
        static Random random = new Random();
        public static int[,,] Rnd3DArray(int xLength, int yLength, int zLength)
        {
            int[,,] array = new int[xLength, yLength, zLength];

            for (int x = 0; x < xLength; x++)
            {
                for (int y = 0; y < yLength; y++)
                {
                    for (int z = 0; z < zLength; z++)
                    {
                        array[x, y, z] = random.Next(0, 3);
                    }
                }
            }
            return array;
        }
        public static void DisplayHighlight(int[,,] array, int xMark, int yMark, int zMark)
        {
            Console.Clear();

            ConsoleColor previousForegroundColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.DarkGray;
            for (int z = 0; z < array.GetLength(2); z++)
            {
                Console.WriteLine("Layer {0}:", z);

                for (int y = 0; y < array.GetLength(1); y++)
                {
                    for (int x = 0; x < array.GetLength(0); x++)
                    {
                        if (y == yMark && x == xMark && z == zMark)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                        }
                        Console.Write(array[x, y, z] + " ");
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                    }
                    Console.WriteLine();
                }

                if (z < array.GetLength(2) - 1)
                    Console.WriteLine("\n");
                else
                    Console.WriteLine();
            }

            Console.ForegroundColor = previousForegroundColor;
        }
    }
}