﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;

#region Feladatleírás
/*
3. feladat

Írj metódust, mely kiszámolja, hogy egy adott K x L x M-es három dimenziós,
egész számokat tartalmazó számrácsban mennyi a 3x3x3 méretű részkockákban előforduló
27 szám szorzatának maximuma az összes ilyen részkockát tekintve.Ha valamelyik dimenzió kisebb 3-nál,
akkor 0 legyen az eredmény.A metódus a számrácsot leíró fájl elérési útját kapja paraméterül,
mely a következő formátumú:

K L M

A(111) A(211) … A(K11)
A(121) A(221) … A(K21)
…
A(1L1) A(2L1) … A(KL1)

A(112) A(212) … A(K12)
A(122) A(222) … A(K22)
…
A(1L2) A(2L2) … A(KL2)

…

A(11M) A(21M) … A(K1M)
A(12M) A(22M) … A(K2M)
…
A(1LM) A(2LM) … A(KLM)
Példa:

4 3 2

1 2 3 4
5 6 7 8
9 10 11 12

13 14 15 16
17 18 19 20
21 22 23 24

A beadandó osztály ne tartalmazzon külső függőséget, és a következő interfészt valósítsa meg:

interface IMaxProduct
{
    BigInteger GetMax3x3x3ProductInCuboid(string filePath);
}

Értékkorlátok:
0 <= K,L,M <= 1000
*/
#endregion

namespace F3_3Dtombos
{
    class OzsvartKaroly_F3 : IMaxProduct
    {
        //OLVASD EL:
        //A feladatleírás nem adta meg (pedig jó lett volna! -.-)
        //hogy a számrácsban található egész számok milyen nagyok lehetnek,
        //de remélem az int elég nagy lesz.

        object lockObject = new object();
        int[,,] matrix;
        int K;
        int L;
        int M;
        string filePath;
        BigInteger max = 0;

        public BigInteger GetMax3x3x3ProductInCuboid(string filePath)
        {
            this.filePath = filePath;
            ValidateAndSetKLM();
            if (K < 3 || L < 3 || M < 3)
            {
                return BigInteger.Zero;
            }

            ReadFileToMatrix();
            CalculateResult();
            return max;
        }
        BigInteger GetSub3x3ElementsPRODUCT(int centerX, int centerY, int centerZ)
        {
            return
                //HORIZONTAL LAYER TO THE LEFT   (centerX - 1)
                (BigInteger)matrix[centerX - 1, centerY - 1, centerZ - 1] *
                matrix[centerX - 1, centerY - 1, centerZ] *
                matrix[centerX - 1, centerY - 1, centerZ + 1] *

                matrix[centerX - 1, centerY, centerZ - 1] *
                matrix[centerX - 1, centerY, centerZ] *
                matrix[centerX - 1, centerY, centerZ + 1] *

                matrix[centerX - 1, centerY + 1, centerZ - 1] *
                matrix[centerX - 1, centerY + 1, centerZ] *
                matrix[centerX - 1, centerY + 1, centerZ + 1] *

                //HORIZONTAL MIDDLE LAYER        (centerX)
                matrix[centerX, centerY - 1, centerZ - 1] *
                matrix[centerX, centerY - 1, centerZ] *
                matrix[centerX, centerY - 1, centerZ + 1] *

                matrix[centerX, centerY, centerZ - 1] *
                matrix[centerX, centerY, centerZ] *
                matrix[centerX, centerY, centerZ + 1] *

                matrix[centerX, centerY + 1, centerZ - 1] *
                matrix[centerX, centerY + 1, centerZ] *
                matrix[centerX, centerY + 1, centerZ + 1] *

                //HORIZONTAL LAYER TO THE RIGHT  (centerX + 1)
                matrix[centerX + 1, centerY - 1, centerZ - 1] *
                matrix[centerX + 1, centerY - 1, centerZ] *
                matrix[centerX + 1, centerY - 1, centerZ + 1] *

                matrix[centerX + 1, centerY, centerZ - 1] *
                matrix[centerX + 1, centerY, centerZ] *
                matrix[centerX + 1, centerY, centerZ + 1] *

                matrix[centerX + 1, centerY + 1, centerZ - 1] *
                matrix[centerX + 1, centerY + 1, centerZ] *
                matrix[centerX + 1, centerY + 1, centerZ + 1];
        }

        /// <summary>
        /// Assuming that the "matrix" is already set, calculates the result.
        /// </summary>
        void CalculateResult()
        {
            int xLength = matrix.GetLength(0) - 1;
            int yLength = matrix.GetLength(1) - 1;
            int zLength = matrix.GetLength(2) - 1;

            List<Task> tasks = new List<Task>();

            for (int z = 1; z < zLength; z++)
            {
                for (int y = 1; y < yLength; y++)
                {
                    int zLocal = z; //avoiding outer variable trap
                    int yLocal = y; //avoiding outer variable trap
                    tasks.Add(Task.Factory.StartNew(() =>
                    {
                        for (int x = 1; x < xLength; x++)
                        {
                            BigInteger sub3x3Prod = GetSub3x3ElementsPRODUCT(x, yLocal, zLocal);
                            lock (lockObject)   //avoiding false "maximums" 
                            {
                                if (sub3x3Prod > max)
                                    max = sub3x3Prod;
                            }
                        }
                    }
                        ));
                }
            }

            Task.WaitAll(tasks.ToArray());
        }

        /// <summary>
        /// Assuming that K,L,M and filePath are already set, reads the file and sets the "matrix"
        /// </summary>
        /// <returns></returns>
        void ReadFileToMatrix()
        {
            matrix = new int[K, L, M];

            int z = -1;
            using (StreamReader sr = new StreamReader(filePath))
            {
                sr.ReadLine(); //First row => "K L M" => I don't care
                string row = sr.ReadLine();
                #region Skip empty rows
                while (string.IsNullOrWhiteSpace(row))
                {
                    row = sr.ReadLine();
                }
                #endregion

                while (!sr.EndOfStream)
                {
                    z++;
                    if (z >= M)
                    {
                        break;
                    }
                    #region Skip empty rows
                    while (string.IsNullOrWhiteSpace(row))
                    {
                        row = sr.ReadLine();
                    }
                    #endregion

                    //Process current layer:
                    for (int y = 0; y < L; y++)
                    {
                        string[] rowContents = row.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries); //If there are more spaces than necessary, ignore them
                        for (int x = 0; x < K; x++)
                        {
                            matrix[x, y, z] = int.Parse(rowContents[x]);
                        }

                        row = sr.ReadLine();
                        #region Skip empty rows
                        while (string.IsNullOrWhiteSpace(row))
                        {
                            if (row == null) //end of file
                            {
                                break;
                            }
                            row = sr.ReadLine();
                        }
                        #endregion
                    }
                }
            }
        }

        /// <summary>
        /// Validates and sets the values of K, L and M. If any is not valid, throws an exception.
        /// </summary>
        /// <param name="filePath"></param>
        void ValidateAndSetKLM()
        {
            using (StreamReader sr = new StreamReader(filePath))
            {
                string[] KLMstrings = sr.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                K = int.Parse(KLMstrings[0]);
                L = int.Parse(KLMstrings[1]);
                M = int.Parse(KLMstrings[2]);

                if (K < 0 || K > 1000 || L < 0 || L > 1000 || M < 0 || M > 1000)
                {
                    throw new ApplicationException("K, L, M should be bigger or equal than zero and less or equal than 1000.");
                }
            }
        }
    }
}
interface IMaxProduct
{
    BigInteger GetMax3x3x3ProductInCuboid(string filePath);
}